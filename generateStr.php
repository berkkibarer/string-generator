<?php
/********GNU LICENSE***********/
//    This file is part of String Generator.
//
//    String Generator is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    String Generator is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with String Generator.  If not, see <http://www.gnu.org/licenses/>.

/********CREDITS*********/
/*29.11.2016 created by Berk Kibarer*/
/*30.11.2016 UTF-8 support added by Orkun Karaduman*/

  if(isset($argv[1]) && $argv[1]=="help"){
 	$manData = '
/*********LICENSE********/
/*This software is licensed under GNU GPL v3. license*/

/********CREDITS*********/
/*29.11.2016 created by Berk Kibarer*/
/*30.11.2016 UTF-8 support added by Orkun Karaduman*/

Usage: php generateStr.php [args...]

Sample Usage: php generatorStr.php 10 alpha

First Argument: Length of the string (*)

Second Argument: Combination (alpha default)
	- alpha
	- alphaTR
	- num
	- special
	- alphaNum
	- alphaNumTR
	- alphaNumSpecial
	- alphaNumSpecialTR

';
	echo $manData;
	exit;
  }
  $length = isset($argv[1]) ? $argv[1] : 10;
  $style = isset($argv[2]) ? $argv[2] : "alpha";
	
  $alphaNumSpecial = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
  $num = "0123456789";
  $alpha = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $special = "!@#$%^&*()_-=+;:,.?";
  $alphaNum = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  $alphaTR = 'abcdefghijklmnopqrstuvwxyzıöüşçğABCDEFGHIJKLMNOPQRSTUVWXYZİÖÜĞŞÇ';
  $alphaNumTR = "abcdefghijklmnopqrstuvwxyzıöüşçğABCDEFGHIJKLMNOPQRSTUVWXYZİÖÜĞŞÇ0123456789";
  $alphaNumSpecialTR = "abcdefghijklmnopqrstuvwxyzıöüşçğABCDEFGHIJKLMNOPQRSTUVWXYZİÖÜĞŞÇ0123456789!@#$%^&*()_-=+;:,.?";
	
   $combination= $$style;
   $createdStr = '';

   for ($i = 0; $i < $length; $i++) {
       $createdStr .= mb_substr($combination,rand(0, mb_strlen($combination)-1),1,'UTF-8');
   }

   echo "\nString Count: ".mb_strlen($createdStr)."\n\n". $createdStr."\n\n";
?>
